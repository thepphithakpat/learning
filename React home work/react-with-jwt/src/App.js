import Login from "./components/Login";
import React, { useState } from 'react'
import Welcome from "./components/Welcome";

function App() {
  const [pass, setPass] = useState(false)
  const [userInfo, setUserInfo] = useState({})
  return (
    <div className="App">
      {
        pass ? <Welcome userInfo={userInfo} setPass={setPass}/> : <Login setPass={setPass} setUserInfo={setUserInfo} />
      }
    </div>
  );
}

export default App;
