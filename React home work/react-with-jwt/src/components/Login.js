import { useState } from "react"
import "../App.css"
import ModalMsg from "./ModalMsg"
export default function Login(props) {
    let userInfo = {}
    const [incorrectAuth, SetIncorrectAuth] = useState(false)
    const handleUsername = (e) => {
        //console.log('USERNAME : ', e.target.value);
        userInfo.username = e.target.value
    }

    const handlePassword = (e) => {
        //console.log('PASSWORD : ', e.target.value);
        userInfo.password = e.target.value
    }

    const genToken = async () => {
        try {
            const response = await fetch(`http://localhost:3001/login`, {
                method: 'GET',
                mode: 'cors',
                headers: {
                    'username': userInfo.username,
                    'password': userInfo.password
                }
            })
            const data = await response.json()

            console.log(data);

            localStorage.setItem('TOKEN', data.token)
        } catch (error) {
            console.error(error);
        }
    }

    const authorization = async () => {
        try {
            const response = await fetch(`http://localhost:3001/auth`, {
                method: 'GET',
                headers: {
                    'Authorization': localStorage.getItem('TOKEN')
                }
            })
            console.log('STATUS : ', response.status);
            if (response.status === 401) {
                SetIncorrectAuth(true)
            }
            const state = await response.json()
            console.log('pass : ', state.pass);
            return response.status === 200 ? true : false;
        } catch (error) {
            console.error(error);
        }
    }

    const authorizationProcess = async () => {
        try {
            await props.setUserInfo(userInfo)
            await genToken()
            const isPass = await authorization()
            props.setPass(isPass)
        } catch (error) {
            console.error(error);
        }
    }

    const loginHandle = () => {
        //console.log('USER INFO : ', userInfo);
        authorizationProcess()
    }

    return (
        <div className="container container-login">
            <div className="row d-flex align-items-center login-group">

                <div className="container mt-3 login-box border border-info">
                    <div className="row p-5">
                        {incorrectAuth ? <ModalMsg /> : <div></div>}

                        <div className="row login-header mb-3 justify-content-center ">
                            Login
                        </div>
                        <div className="row mb-3">
                            <input id="username" className="form-control App" placeholder="Username" onChange={handleUsername} />
                        </div>
                        <div className="row mb-3">
                            <input id="password" type={'password'} className="form-control App" placeholder="Password" onChange={handlePassword} />
                        </div>
                        <div className="row mb-3">
                            <button className="btn btn-primary" onClick={loginHandle}>Login</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
} 