import React from 'react'
import bootstrap from '../../node_modules/bootstrap/dist/js/bootstrap.bundle.js'

export default function ModalMsg() {
    return (
        <div className='liveAlertPlaceholder'>
            <div className="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Warning!</strong> user or password is incorrect.  Try again, please
            </div>
        </div>
    )
}
