import React, { useEffect, useState } from 'react'

export default function Welcome(props) {
  const [date, setDate] = useState('')

  let dateTime = () => {
    try {
      return new Date().toString();
    } catch (error) {
      throw new Error()
    }
  }

  useEffect(() => {
    const intervalId = setInterval(() => {
      setDate(new Date().toString())
    }, 1000)
    return () => {
      clearInterval(intervalId)
    };
  }, []);

  const hadleBack = (e) => {
    props.setPass(false)
  }
  return (
    <div className='container'>
      <div className='row mt-3'>
        <div className='col'>
          <div className="alert alert-primary" role="alert">
            Welcome <span style={{ fontWeight: 'bold' }}>{props.userInfo.username}</span>
          </div>
        </div>
      </div>

      <div className='row mt-3 justify-content-center'>
        <div className="card" style={{ width: '25rem' }}>
          <img src="https://www.bsglobaltrade.com/wp-content/uploads/2016/09/person-icon.png" className='card-img-top' alt="..."></img>
          <div className="card-body">
            <p className="card-text">{date
            }</p>
          </div>
        </div>
      </div>

      <div className='row mt-3'>
        <button className='btn btn-secondary' onClick={hadleBack}>Logout</button>
      </div>
    </div>
  )
}
