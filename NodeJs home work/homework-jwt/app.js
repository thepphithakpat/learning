const express = require('express')
const bodyParser = require('body-parser')
const jwt = require("jsonwebtoken");
var cors = require('cors');
const passport = require("passport");
const ExtractJwt = require("passport-jwt").ExtractJwt;
const JwtStrategy = require("passport-jwt").Strategy;
const SECRET = "MY_SECRET_KEY";
const BEARER = 'Bearer '

// initail
const app = express()
const port = 3001;
app.use(bodyParser.json());
app.use(cors());

//gen token
const loginMiddleWare = (req, res, next) => {
    if (req.get('username') === "pluem" && req.get('password') === "pluem") {
        next();
    } else {
        return res.status(401).json({
            code: 1,
            msg: 'Wrong username and password'
        })
    }
};

app.get('/login', loginMiddleWare, (req, res, next) => {
    try {
        const payload = {
            sub: req.get('username'),
            iat: new Date().getTime()
        };
        const SECRET = "MY_SECRET_KEY";
        const token = jwt.sign(payload, SECRET);
        return res.status(200).json({
            code: 1,
            token: token
        });
    } catch (error) {
        console.error(error);
        throw new Error(error.msg)
    }
})

//authen
const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader("authorization"),
    secretOrKey: SECRET,
}

const jwtAuth = new JwtStrategy(jwtOptions, (payload, done) => {
    if (payload.sub === "pluem") done(null, true);
    else done(null, false);
});

passport.use(jwtAuth);

const requireJWTAuth = passport.authenticate("jwt", { session: false });

app.get("/auth", requireJWTAuth, (req, res) => {
    try {
        return res.status(200).json({
            code: 1,
            pass: true
        });
    } catch (error) {
        throw new Error(error.msg)
    }
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`);
});